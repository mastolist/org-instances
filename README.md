
# An unofficial list of official Mastodon instances

This is an unofficial list of organisations that have set up a Mastodon
instance under their own brand / identity, using their primary domain name.

This means:
* Yes --> social.example.com
* No --> example.social

See also: [Your organization should run its own Mastodon server](https://martinfowler.com/articles/your-org-run-mastodon.html) (Martin Fowler)

**Last update**: 2022-12-17

---

## Public sector
* European Union: [https://social.network.europa.eu](https://social.network.europa.eu/explore)
* German government: [https://social.bund.de](https://social.bund.de)

## Political parties
* Fraktion DIE LINKE (Germany) [https://social.linksfraktion.de/](https://social.linksfraktion.de/explore)

## Education
* ELTE (HU): [https://mastodon.elte.hu](https://mastodon.elte.hu/explore)

## Non-profit / Social profit / NGO
* The Internet Archive: [https://mastodon.archive.org](https://mastodon.archive.org/explore)
* OER (Open Educational Resource) Foundation: [https://mastodon.oeru.org](https://mastodon.oeru.org/explore)
* Open Collective: [https://mastodon.opencollective.com](https://mastodon.opencollective.com/explore)
* Open Rights Group: [https://social.openrightsgroup.org](https://social.openrightsgroup.org/explore)

## Opensource projects
* ...

## Media
* Mac Stories: [https://mastodon.macstories.net](https://mastodon.macstories.net/explore)
* Tuscon Sentinel: [https://mastodon.tucsonsentinel.com](https://mastodon.tucsonsentinel.com/explore)
 

## Corporate / commercial / other
* Cloudron: [https://social.cloudron.io](https://social.cloudron.io/explore)
* Factorial: [https://social.factorial.io](https://social.factorial.io/explore)
* Thoughtworks: [https://toot.thoughtworks.com](https://toot.thoughtworks.com/explore)
* Village One: [https://toot.village.one](https://toot.village.one/explore)

---

## On org domain but not officially sanctioned
* MIT (USA): [https://mastodon.mit.edu](https://mastodon.mit.edu/explore)

---

## Contributing

Please open an issue, or fork the repo and submit a merge request.

## Credits

Created and curated by [@jpoesen@social.coop](https://social.coop/@jpoesen).
